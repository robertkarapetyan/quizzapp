import React, { useState, useEffect } from 'react';
import './index.css';
import Select from '@mui/material/Select'
import { MenuItem } from '@mui/material';
import CuontrollButton from './ControllButton.js'
import StyledSelect from './StyledSelect.js'


function StartPage({
    sendFetchResultJson,
    start,
}) {
    const [category, setCategory] = useState(0);
    // const [isOpened, setOpened] = useState(false);

    const getData = async () => {
        let result = await fetch(`https://opentdb.com/api.php?amount=10&category=${category}`) //difficulty=easy&&type=multiple
        let data = await result.json();
        sendFetchResultJson(data);
    };

    useEffect(() => {
        if (category !== 0) {
            getData();
        }
        // if(category === 18){   // close menu only if input was 18
        //     console.log(setOpened(false));
        // }
        // console.log("category    --===: " + category);
    }, [category]);

    const handleCategoryChange = (event) => {
        setCategory(event.target.value);
    }

    // const onCloseMenu = (event) => {
    //     // if(category === 0){
    //     //     setOpened(false)
    //     //     return;
    //     // }
    //     event.stopPropagation();
    //     event.preventDefault();
    // }
    // const onOpenMenu = (event) => {
    //     setOpened(true);
    // }

    const sStyle = {
        input: {
            sx: {
                width: "200px",
                backgroundColor: "green",
            },
        }
    }

    return (
        <div className="Page">
            <h1 className="heading1">Trivia App</h1>
            <h2 className="heading2"> Pick a Category </h2>

                <StyledSelect displayEmpty
                styles={sStyle}
                    defaultValue=""
                    onChange={handleCategoryChange}
                // {    filter input         ||     menuitem style disabled as regular
                // onClose={onCloseMenu}
                // onOpen={onOpenMenu}
                // open={isOpened}
                //  }   ------------
                    sx={{width: "10%", minWidth: "300px", marginTop: "20px"}}
                >
                    <MenuItem value="" disabled sx={{ display: "none" }} root="true">Category</MenuItem>
                    <MenuItem value={17} disabled>Science and Nature</MenuItem>
                    <MenuItem value={18} >Science: Computers</MenuItem>
                    <MenuItem value={19} disabled>Science: Mathematics</MenuItem>
                </StyledSelect>
            <CuontrollButton
                style={{ marginTop: "50px" }}
                disabled={category ? false : true}
                onClick={start}
            >
                Start
            </CuontrollButton>
        </div>
    )
}

export default StartPage;


// import TextField from "@material-ui/core/TextField";
// import ControllButton from './ControllButton.js'
// // import { withStyles } from '@material-ui/core/styles';
// import MenuList from '@mui/material/MenuList';
// import { createTheme } from '@mui/material/styles';
// import { makeStyles } from "@material-ui/core/styles";
// import purple from '@mui/material/colors/purple';

// import { ThemeProvider as MuiThemeProvider } from '@mui/material/styles';
// import { MuiThemeProvider } from "@material-ui/core/styles";

// const MyTheme = createTheme({
//     palette: {
//         primary: {
//             light: '#00ff00',
//             main: '#95b6a9',
//             dark: '#FF0000'
//         },
//         secondary: { 
//             light: '#00ff00',
//             main: "#ff0000",
//             dark: '#FF0000'
//         },
//         action: {
//             selected: '#00ff00',
//             hover: '#FFD371',   // yellow
//             disabled: '#0000ff'
//         }
//     },
    
    // overrides: {
    //     '&.MuiMenuItem-root': {
    //         border: '5px',
    //         borderColor: 'orange',
    //         backgroundColor: "green"
    //     },
    //     '&.Mui-selected': {
    //         backgroundColor: "red"

    //     },
    //     '&.Mui-selected:hover': {
    //         backgroundColor: "black"

    //     },
    //     // MuiMenuItem: {
    //     //     root: {
    //     //         '&.MuiMenuItem-root': {
    //     //             border: '5px',
    //     //             borderColor: 'orange',
    //     //             backgroundColor: "green"
    //     //         },
    //     //         '&.Mui-selected': {
    //     //             backgroundColor: "red"

    //     //         },
    //     //         '&.Mui-selected:hover': {
    //     //             backgroundColor: "black"

    //     //         },
    //     //         '&.Mui-disabled': {
    //     //             border: '5px',
    //     //             borderColor: 'orange',
    //     //             backgroundColor: "green",
    //     //             opacity: '1',
    //     //         },
    //     //         "&:hover": {
    //     //             "width": "90%",
    //     //             "heigth": "90%",
    //     //             "border": "10px",
    //     //             "border-color": "orange",
    //     //             "border-radius": "10px 10px 10px 10px",

    //     //             backgroundColor: "blue"
    //     //         },
    //     //     },
    //     // },
    // }
// });

// const menuStyle = {
//     marginTop: "20px",
//     width: "30%",
//     alignSelf: "center",
//     input: {        ////    --------=== root styling ===-------
//         sx: {
//             width: '200px',
//             backgroundColor: { xs: "#ff0000", sm: "#fff000" },
//         },
//     }
//     // backgroundColor: { pm: "#ff0000", sm: "#00fff0" },
//     // border: "solid black 10px",
//     // '&:before': {
//     //     borderColor: "#ff0000",
//     // },
//     // '&:after': {
//     //     borderColor: "#ff0000",
//     // },
//     // "& ul": {
//     //     backgroundColor: "#cccccc",
//     // },
//     // borderColor: {xs: "#ff0000", sm: "#fff000"},
// }


// const useStyles = makeStyles( theme => ({  //legacy
//     testText:{
//         border: "4px solid red",
//     }
// }));
// const classes = useStyles();
