import { styled } from '@mui/system';
import Select from "@mui/material/Select";
import { MenuItem } from '@mui/material';
import './index.css';


import { ThemeProvider as MuiThemeProvider } from '@mui/material/styles';
import { createTheme } from '@mui/material/styles';

// const SelectStyleOverrideTheme = createTheme({
//     components: {
//         MuiSelect: {
//             root: {
//                 backgroundColor: "red"
//             }
//         }
//     }
// })


// function StyledSelect() {

//     return (
//         <MuiThemeProvider theme={SelectStyleOverrideTheme}>
//             <Select displayEmpty
//                 defaultValue=""
//             >
//                 <MenuItem value="" disabled sx={{ display: "none" }} root="true">Category</MenuItem>
//                 <MenuItem value={17} >Science and Nature</MenuItem>
//                 <MenuItem value={18} >Science: Computers</MenuItem>
//                 <MenuItem value={19} >Science: Mathematics</MenuItem>
//             </Select>
//         </MuiThemeProvider>
//     )
// }
const StyledSelect = styled(Select)`
// border: 2px solid red;
    border-radius: 15px;
    margin-top: 20px;
    min-width: 320px;
    & $notchedOutline {
        borderColor: yellow;
      }
//     &.MuiFormControl-root{
//         color: red
//     }
//   &.Mui-selected {
//     background-color: red
//     }
//   &.MuiMenuItem-root {
//     border: 5px solid orange;
//     border-color: orange;
//     background-color: red;
//   }
//   &.MuiOutlinedInput-root{
//       border: 10px solid yellow;
//       color: purple;
//       border-radius: 15px;
//       background-color: green;
//       width: 400px;
//   }
//   &.MuiList-root{
//       background-color: blue;
//   }
//   &.MuiPaper-root {
//     background-color: red;
//   }
//   &.MuiTouchRipple-root{
//       background-color: green;
//       color: red;
//   }
//   &.Mui-list{
//       background-color: red;
//   }
}
`;

export default StyledSelect;

// palette: {
//     primary: {
//         light: '#00ff00',
//         main: '#95b6a9',
//         dark: '#FF0000'
//     },
//     secondary: { 
//         light: '#00ff00',
//         main: "#ff0000",
//         dark: '#FF0000'
//     },
//     action: {
//         selected: '#00ff00',
//         hover: '#FFD371',   // yellow
//         disabled: '#0000ff'
//     }
// },

