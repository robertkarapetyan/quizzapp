import React from 'react';
import { Markup } from 'interweave';
import AnswerButton from './AnswerButton.js'

function ListItem({
    isCorrect,
    isUserInput,
    click,
    value,
    completedStatus,
}) {
    let buttonStyle = {};
    if(completedStatus){
        buttonStyle = {
            backgroundColor: isCorrect ? "#40f149" : (isUserInput ? "#f14040" : ""),
            color: isCorrect || isUserInput? "black" : "",
            border: isCorrect || isUserInput? "none" : "",
            cursor: "default"
        }
    }
    return (
        <li >
            <AnswerButton style={buttonStyle} onClick={(e) => click(e, value)}
            >
            <Markup content={value}/>
            </AnswerButton>
        </li >
    );
}

export default ListItem;
