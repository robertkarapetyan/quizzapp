import './App.css';
import React, { useState, useEffect } from 'react';
import QuestionPage from './QuestionPage.js'
import StartPage from './StartPage.js'
import CuontrollButton from './ControllButton.js'


function App() {
  const [data, setData] = useState([]);
  const [selectedPageIndex, setSelectedPageIndex] = useState(-1);
  const [score, setScore] = useState(0);
  const [userAnswers, setUserAnswers] = useState([]);
  const [completedStatus, setCompletedStatus] = useState(false); // ongoing || completed
  // const [shuffledAnswerOptions, setShuffledAnswerOptions] = useState([]); 
  
  useEffect(() => {
    if (selectedPageIndex >= 0) {  //stepForward
      setSelectedPageIndex(selectedPageIndex + 1);
    }
    if (userAnswers.length === data.length && data.length !== 0) {
      setCompletedStatus(true);
    }
  }, [userAnswers])

  const handleUserAnswer = (result, value) => {
    if (result === true) {
      setScore(score + 1);
    }
    setUserAnswers(userAnswers => [...userAnswers, value]);
  }

  const skipPage = () => {
    if (completedStatus === true || selectedPageIndex === -1) {
      setSelectedPageIndex(selectedPageIndex + 1);
      return;
    }
    setUserAnswers(userAnswers => [...userAnswers, "none"]);
  }

  const moveBack = () => {
    setSelectedPageIndex(selectedPageIndex - 1);
  }

  const reviewAnswers = () => {
    setSelectedPageIndex(0);
  }

  const resetData = () => {
    // console.log("     ---=== resetData  ===---  ");
    setSelectedPageIndex(-1);
    setData([]);
    setScore(0);
    setUserAnswers([]);
    setCompletedStatus(false);
  }

  if (selectedPageIndex < 0) {
    return (
      <StartPage
        sendFetchResultJson={(dataJson) => setData(dataJson.results)}
        start={skipPage}
      />
    )
  }
  else if (selectedPageIndex >= 0 && selectedPageIndex < data.length) {
    return (
      <QuestionPage
        index={selectedPageIndex}
        question={data[selectedPageIndex] ? data[selectedPageIndex].question : "question null"}
        difficulty={data[selectedPageIndex] ? data[selectedPageIndex].difficulty : "difficulty null"}
        correctAnswer={data[selectedPageIndex] ? data[selectedPageIndex].correct_answer : "correctAnswer null"}
        incorrectAnswers={data[selectedPageIndex] ? data[selectedPageIndex].incorrect_answers : []}
        passResult={handleUserAnswer}
        stepForward={skipPage}
        stepBackward={moveBack}
        completedStatus={completedStatus}
        resetData={resetData}
        userAnswer={completedStatus ? userAnswers[selectedPageIndex] : undefined}
      />
    )
  }
  else {
    console.log("completed: " + completedStatus)
    console.log("userAnswers.length: " + userAnswers)
    return (
      <div className="Page">
        <h1 className="heading1">Thank You!</h1>
        <h2 className="heading2">Your score is: {score} / {data.length}</h2>
        <div style={{ alignSelf: "center", display: "flex", marginTop: "30px", gap: "30px" }}>
          {
            // render previous button only if game completedStatus is true
            !!completedStatus
            && (
              <CuontrollButton
                onClick={reviewAnswers}
              >
                Review answers
              </CuontrollButton>
            )
          }
          <CuontrollButton onClick={resetData}>
            Home
          </CuontrollButton>
        </div>
      </div>
    )
  }
}

export default App;