import React, { useEffect } from 'react';
import AnswerList from './AnswerList.js'
import './index.css';
import { Markup } from 'interweave';
import CuontrollButton from './ControllButton.js'
import { CSSTransition, TransitionGroup } from 'react-transition-group'

function QuestionPage({
    index,
    question,
    difficulty,
    correctAnswer,
    incorrectAnswers,
    passResult,
    stepForward,
    stepBackward,
    completedStatus,
    resetData,
    userAnswer,
}) {
    let questionsArray = incorrectAnswers.concat(correctAnswer);
    const questionsShuffledArr = questionsArray.sort(() => .5 - Math.random());


    const handleClick = (e, value) => {
        console.log(45)
        passResult(value === correctAnswer, value);
    }

    let difficultyColor = "";
    switch (difficulty) {
        case "easy": {
            difficultyColor = "#42a976"
            break;
        }
        case "medium": {
            difficultyColor = "#eac505"
            break;
        }
        case "hard": {
            difficultyColor = "#ef7d54"
            break;
        }
        default: break;
    }

    if (questionsShuffledArr.length > 0) {
        return (
            <TransitionGroup>
                <CSSTransition
                    appear={true}
                    key={index}
                    timeout={500}
                    classNames="fade"
                >
                    <div className="questionContainer" disabled>
                        <h1 className="heading1"> Question {index + 1} </h1>
                        <p className="DifficultyLevel" style={{
                            backgroundColor: difficultyColor,
                        }}>
                            {difficulty}
                        </p>
                        <h2 className="heading2">
                            <Markup content={question} />
                        </h2>
                        {
                            // render 'skipped answer indicator' only if user answer was "none"
                            (userAnswer === "none" ?
                                <h3 style={{ color: "#ec4641" }}>You've skipped this question.</h3> :
                                <> </>)
                        }
                        <AnswerList
                            answersArray={questionsShuffledArr}
                            completedStatus={completedStatus}
                            handleClick={handleClick}
                            correctAnswer={correctAnswer}
                            userInput={userAnswer}
                        />
                        <div style={{ display: "flex", marginTop: "30px", gap: "30px", alignSelf: "center" }}>
                            {
                                // render 'previous' button only if game completedStatus is true
                                !!completedStatus
                                && (
                                    <CuontrollButton
                                        onClick={index === 0 ? resetData : stepBackward}
                                    >
                                        {index === 0 ? "Home" : "Previous"}
                                    </CuontrollButton>
                                )
                            }
                            <CuontrollButton
                                onClick={stepForward}
                            >
                                {completedStatus === true ? "Next" : "Skip"}
                            </CuontrollButton>
                        </div>
                    </div>
                </CSSTransition>
            </TransitionGroup>
        );
    }
    return null;
}

export default QuestionPage;

/* <TransitionGroup>
                <CSSTransition
                    key={index}
                    timeout={3000}
                    clasNames="fade"
                ></CSSTransition>
                                </CSSTransition>
            </TransitionGroup> */