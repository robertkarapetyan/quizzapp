import React from 'react';
import ListItem from './ListItem.js'

function Answerlist({
    answersArray,
    completedStatus,
    handleClick,
    correctAnswer,
    userInput,
}) {
    return (
        <ul style={{
            display: "flex",
            flexDirection: "row",
            listStyleType: "none",
            padding: "0",
            justifyContent: "space-around",
            alignSelf: "center",
            gap: "20px"
        }}
        >
            {   
                answersArray.map(
                    (answer) => (
                    <ListItem
                        isCorrect={answer === correctAnswer}
                        isUserInput={answer === userInput}
                        completedStatus={completedStatus}
                        click={completedStatus? () =>{} : handleClick}
                        value={answer}
                        key={answer}>
                    </ListItem>)
                )
            }
        </ul>
    );
}

export default Answerlist;