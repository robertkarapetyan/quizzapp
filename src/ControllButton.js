import * as React from 'react';
import Stack from '@mui/material/Stack';
import ButtonUnstyled, { buttonUnstyledClasses } from '@mui/base/ButtonUnstyled';
import { styled } from '@mui/system';

const ControllStyledButton = styled('button')`
  background-color: #3a7859;
  padding: 15px 20px;
  border-radius: 15px;
  color: #fff;
  font-weight: 600;
  transition: all 200ms ease;
  border: none;
  min-width: 120px;
  min-height: 40px;
  align-self: center;

  &:hover {
    background-color: #2d6a4c;;
  }

  &.${buttonUnstyledClasses.disabled} {
    color: #c4c4c4;
    background-color: #f6f7f8;
  }
`;

function CuontrollButton(props) {
    return <ButtonUnstyled {...props} component={ControllStyledButton} />;
}

export default CuontrollButton;