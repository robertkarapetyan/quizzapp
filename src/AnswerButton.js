import * as React from 'react';
import ButtonUnstyled from '@mui/base/ButtonUnstyled';
import { styled } from '@mui/system';

const AnswerStyledButton = styled('button')`
  border: 2px solid #95b6a9;
  background-color: #eff2f7;
  padding: 15px 20px;
  border-radius: 15px;
  color: #354153;
  font-weight: 600;
  transition: all 200ms ease;
  cursor: pointer;
  min-width: 200px;
  min-height: 40px;

  &:hover {
    color: #3a7859;
    border-color: #d4e0e0;
    background-color: #d4e0e0;
  }
`;

function AnswerButton(props) {
  return <ButtonUnstyled {...props} component={AnswerStyledButton} />;
}

export default AnswerButton;
// export default function ControllButton() {
//   return (
//     <CustomButton>Button</CustomButton>
//   );
// }